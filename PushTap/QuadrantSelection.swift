//
//  QuadrantSelection.swift
//  PushTap
//
//  Created by Conor Pierce on 4/7/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit

func getPosition() -> CGPoint{
    let userDefaults = UserDefaults.standard
    let lastQuadrant = userDefaults.integer(forKey: "LastQuadrant")
    var circlePosition: CGPoint = CGPoint(x: 0, y: 0)
    var newQuadrant = Int.random(in: 1..<5)
    //print(newQuadrant)
    while( newQuadrant == lastQuadrant){
        newQuadrant = Int.random(in: 1..<5)
    }
    userDefaults.set(newQuadrant, forKey: "LastQuadrant")
    // print(CGFloat.random(in: 0..<circleXMax!))
    
    switch newQuadrant {
    case 1:
        circlePosition.x = CGFloat.random(in: axisBuffer..<circleXMax!)
        circlePosition.y = CGFloat.random(in: axisBuffer..<circleYMax!)
        //print("Quadrant 1 \(circlePosition)")
        break
    case 2:
        
        circlePosition.x = CGFloat.random(in: axisBuffer..<circleXMax!)
        circlePosition.y = -CGFloat.random(in: axisBuffer..<circleYMax!)
        //print("Quadrant 2 \(circlePosition)")
        break
    case 3:
        circlePosition.x = -CGFloat.random(in: axisBuffer..<circleXMax!)
        circlePosition.y = -CGFloat.random(in: axisBuffer..<circleYMax!)
        //print("Quadrant 3 \(circlePosition)")
        break
    case 4:
        circlePosition.x = -CGFloat.random(in: axisBuffer..<circleXMax!)
        circlePosition.y = CGFloat.random(in: axisBuffer..<circleYMax!)
        //print("Quadrant 4 \(circlePosition)")
        break
    default:
        break
    }
    return circlePosition
}
