//
//  GameScene.swift
//  PushTap
//
//  Created by Conor Pierce on 3/14/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import UIKit
import SpriteKit
import AudioToolbox


protocol GameSceneDelegate: class{
    func segue()
    func showPopOver(score: Int)
}


class GameScene: SKScene, ForceCircleDelegate, StartGameDelegate {
    
    weak var gameDelegate: GameSceneDelegate?
    
    var border1: SKSpriteNode?
    var border2: SKSpriteNode?
    var border3: SKSpriteNode?
    var border4: SKSpriteNode?
    var borders: ForceBorder?
    var scoreCount: Int = 0
    var scoreCounterLabel: SKLabelNode?
    var timer: Timer?
    var difficulty: Double = 1
    var circleCount = 0
    var gameEnded = false
    var holdCounter = 0
    var touchedCircle: SKNode?
    var tapCounter = 0
    
    var currentLevel = 0
    var circleWeights: [Double]?
    var difficultyIncrement: Double?
    var popSpeed: Double?
    var appearSpeed: Double?
    
    
    let gameCircle = GameCircle()
    
    
    var haptic: HapticFeedback?
    
    override func didMove(to view: SKView) {
        
        startGame()
        
    }
    func startGame(){
        getLevelDetails()
        haptic = HapticFeedback()
        var gameCircle = GameCircle(view: self.view!, circleCount: 1)
        
        scoreCounterLabel?.text = "0"
        createForceBorder()
        createScoreCounter()
        
        
//        Need to set the time interval to match the level difficulty!!!
        
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(fire), userInfo: nil, repeats: true)
    }
    
    func getLevelDetails(){
        let userDefaults = UserDefaults.standard
        currentLevel = userDefaults.integer(forKey: "PressedLevelNumber")
        circleWeights = getCircleWeights(levelNumber: currentLevel)
        difficultyIncrement = getDifficultyIncrement(levelNumber: currentLevel)
        popSpeed = getPopSpeed(levelNumber: currentLevel)
        appearSpeed = getAppearSpeed(levelNumber: currentLevel)
    }
    
//    Need to set the time interval to match the level difficulty!!!
    
    
    func increaseSpeed(){
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: appearSpeed! / difficulty, target: self, selector: #selector(fire), userInfo: nil, repeats: true)
    }
    
    
    @objc func fire()
    {
        newCircle()
        circleCount += 1
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch:UITouch = touches.first! as UITouch
        let positionInScene = touch.location(in: self)
        touchedCircle = self.atPoint(positionInScene)
        let gameCircle = GameCircle()
        gameCircle.setTouchedCircle(TouchedCircle: touchedCircle!)
        print(touchedCircle!)
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch:UITouch = touches.first! as UITouch
        let positionInScene = touch.location(in: self)
        let touchedNode = self.atPoint(positionInScene)
        let forceValue = (touch.force/touch.maximumPossibleForce * 100)
        
        //Updating border to match new force values
        updateBorder(Value: forceValue)
        checkCircle(touchedNode: touchedNode, forceValue: forceValue)
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        borders?.updateBorder(ForceValue: 0)
        let touch:UITouch = touches.first! as UITouch
        let positionInScene = touch.location(in: self)
        let touchedNode = self.atPoint(positionInScene)
        if(touchedNode.name == "SingleTap"){
            haptic?.feedback(style: "light")
            touchedNode.removeFromParent()
            scoreCount += 1
            updateScoreCount()
        }
        else if(touchedNode.name == "DoubleTap"){
            if(tapCounter < 1){
                tapCounter += 1
            }
            else{
                haptic?.feedback(style: "light")
                touchedNode.removeFromParent()
                scoreCount += 1
                updateScoreCount()
                tapCounter = 0
            }
        }
        else{
            tapCounter = 0
        }
        print(tapCounter)
    }
    
    
    
    // Creates a new circle for the level, the circles available will depend on level that is currently being played.
    
    func newCircle(){
        let selectedCircle = circleSelect()
        switch selectedCircle {
        case 0:
            let circle = gameCircle.createCircle()
        case 1:
            let circle = gameCircle.createTapCircle()
        case 2:
            let circle = gameCircle.createDoubleTapCircle()
        case 3:
            let circle = gameCircle.createGestureCircle()
        default:
            let circle = gameCircle.createCircle()
        }
        //let circle = gameCircle.createGestureCircle()
        self.addChild(circle)
        gameCircle.startShrink()
        gameCircle.forceCircleDelegate = self
    }
    
    func circleSelect() -> Int{
        
        // Sum of all probabilities (so that we don't have to require that the sum is 1.0):
        let sum = circleWeights!.reduce(0, +)
        // Random number in the range 0.0 <= rnd < sum :
        let rnd = Double.random(in: 0.0 ..< sum)
        // Find the first interval of accumulated probabilities into which `rnd` falls:
        var accum = 0.0
        for (i, p) in circleWeights!.enumerated() {
            accum += p
            if rnd < accum {
                return i
            }
        }
        // This point might be reached due to floating point inaccuracies:
        return (circleWeights!.count - 1)
        //return Int.random(in: 0..<4)
    }
    
    func checkCircle(touchedNode: SKNode, forceValue: CGFloat){
        
        let forceValueInt = Int(forceValue)
        if(touchedNode.name != "GreenCircle" && touchedNode.name != "YellowCircle" && touchedNode.name != "OrangeCircle" && touchedNode.name != "RedCircle"){
            return
        }
        let currentProperties = getCircle(color: touchedNode.name!)
        let currentCircleForce = currentProperties.circleForce
        if(currentCircleForce == 0){
            return
        }
        if(forceValueInt > currentCircleForce! && forceValueInt < currentCircleForce! + 20 && !gameEnded){
            // print("Touched")
            holdCounter += 1
            if(holdCounter == 4){
                haptic?.feedback(style: currentProperties.circleFeedback!)
                touchedNode.removeFromParent()
                scoreCount += 1
                updateScoreCount()
                holdCounter = 0
            }
            
        }
    }
    

    
    func incrementScore(amount: Int){
        scoreCount += amount
        updateScoreCount()
    }
    
    func createForceBorder(){
        borders = ForceBorder.init(view: self.view!)
        border1 = borders!.getBorder1()
        border2 = borders!.getBorder2()
        border3 = borders!.getBorder3()
        border4 = borders!.getBorder4()
        
        self.addChild(border1!)
        self.addChild(border2!)
        self.addChild(border3!)
        self.addChild(border4!)
    }
    func updateBorder(Value: CGFloat){
        borders?.updateBorder(ForceValue: Value)
    }
    func createScoreCounter(){
        scoreCounterLabel = SKLabelNode()
        scoreCounterLabel?.text = "\(scoreCount)"
        scoreCounterLabel?.fontSize = 50
        scoreCounterLabel?.position = CGPoint(x: 0, y: 0)
        scoreCounterLabel?.zPosition = 1
        scoreCounterLabel?.fontColor = .gray
        
        self.addChild(scoreCounterLabel!)
    }
    func updateScoreCount(){
        scoreCounterLabel?.text = "\(scoreCount)"
        
        //Going to change this based off of the current level, I will also change the incremment
        if(scoreCount % 4 == 0){
            difficulty += difficultyIncrement!
            increaseSpeed()
        }
        
    }
    
    func endGame(){
        timer?.invalidate()
        if(gameEnded){
            return
            //Segue to the main menu
        }
        else{
            timer?.invalidate()
            endCircles()
            gameEnded = true
        }
        
    }
    
    func endCircles(){
        let endGameCircles = SKAction.run {
            
            var circle = self.gameCircle.createCircle()
            self.addChild(circle)
            
            let shrink = SKAction.scale(to: 0, duration: 5)
            let removeNode = SKAction.removeFromParent()
            let fadeout = SKAction.fadeAlpha(to: 0, duration: 2)
            
            let sequence = SKAction.sequence([shrink, fadeout, removeNode])
            
            circle.run(sequence)
        }
        let wait = SKAction.wait(forDuration: 0.01)
        
        let sequence = SKAction.sequence([endGameCircles, wait])
        let segueAction = SKAction.run {
            self.gameDelegate?.showPopOver(score: self.scoreCount)
            
        }
        let segueWait = SKAction.wait(forDuration: 5)
        
        let repeatCircles = SKAction.repeat(sequence, count: 100)
        let sequenceSegue = SKAction.sequence([segueAction, repeatCircles])
        self.run(sequenceSegue)
    }
}
