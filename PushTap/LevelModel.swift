//
//  LevelModel.swift
//  PushTap
//
//  Created by Conor Pierce on 3/24/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit

struct LevelModel {
    let image: UIImage
    let number: Int
    let viewBackgroundColor: UIColor
}
