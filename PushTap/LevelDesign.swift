//
//  LevelDesign.swift
//  PushTap
//
//  Created by Conor Pierce on 3/24/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import Foundation


struct levelChar {
    var levelNumber: Int
    var circlePopSpeed: Double
    var oneStar: Int
    var twoStar: Int
    var threeStar: Int
    var circleAppearSpeed: Double
//    var forceAppear = true
//    var forceRate: Int
//    var tapAppear = true
//    var tapRate: Int
//    var doubleTapAppear = true
//    var doubleTapRate: Int
//    var gestureAppear = true
//    var gestureRate: Int
    
}
struct userLevel{
    var levelName: Int
    var passed: Bool
    var stars: Int
    var highScore: Int
}

class levelChars{
    var levelSettings: [levelChar]
    
    init(){
        let userdefaults = UserDefaults.standard
        
        levelSettings = [levelChar(levelNumber: 0, circlePopSpeed: 5.0, oneStar: 20, twoStar: 30, threeStar: 50, circleAppearSpeed: 2.0)]
        
        //Level 1
        let level1 = levelChar(levelNumber: 1, circlePopSpeed: 5.0, oneStar: 20, twoStar: 30, threeStar: 50, circleAppearSpeed: 2.0)
        
        levelSettings.append(level1)
        
        
        
        //Level 2
        let level2 = levelChar(levelNumber: 2, circlePopSpeed: 5.0, oneStar: 20, twoStar: 30, threeStar: 50, circleAppearSpeed: 2.0)
        
        levelSettings.append(level2)
        
        //Level 3
        let level3 = levelChar(levelNumber: 3, circlePopSpeed: 5.0, oneStar: 10, twoStar: 30, threeStar: 50, circleAppearSpeed: 2.0)
        
        levelSettings.append(level3)
        
        //Level 4
        let level4 = levelChar(levelNumber: 4, circlePopSpeed: 5.0, oneStar: 20, twoStar: 30, threeStar: 50, circleAppearSpeed: 2.0)
        
        levelSettings.append(level4)
        
        //Level 5
        let level5 = levelChar(levelNumber: 5, circlePopSpeed: 5.0, oneStar: 20, twoStar: 30, threeStar: 50, circleAppearSpeed: 2.0)
        
        levelSettings.append(level5)
        
        //Level 6
        let level6 = levelChar(levelNumber: 6, circlePopSpeed: 5.0, oneStar: 20, twoStar: 30, threeStar: 50, circleAppearSpeed: 2.0)
        
        levelSettings.append(level6)
        
        //userdefaults.set(levelSettings, forKey: "levelSettings")
        //print(levelSettings)
        
        
        
    }
    func getLevelSetting(level: Int) -> levelChar{
        return levelSettings[level]
    }
    func checkStars(level: Int, score: Int) -> Int{
        if(score >= levelSettings[level].threeStar){
            return 3
        }
        if(score >= levelSettings[level].twoStar){
            return 2
        }
        if(score >= levelSettings[level].oneStar){
            return 1
        }
        return 0
    }
}
