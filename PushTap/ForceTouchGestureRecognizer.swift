//
//  ForceTouchGestureRecognizer.swift
//  PushTap
//
//  Created by Conor Pierce on 3/20/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit.UIGestureRecognizerSubclass

@available(iOS 9.0, *)
final class ForceTouchGestureRecognizer: UIGestureRecognizer {
    
    private let threshold: CGFloat = 0.75
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesBegan(touches, with: event)
        if let touch = touches.first {
            handleTouch(touch)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesMoved(touches, with: event)
        for touch in touches{
            handleTouch(touch)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesEnded(touches, with: event)
        print("Touches Ended Called")
        state = UIGestureRecognizer.State.failed
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesCancelled(touches, with: event)
        state = UIGestureRecognizer.State.failed
        print("Touches Canceled Called")
    }
    
    private func handleTouch(_ touch: UITouch) {
        guard touch.force != 0 && touch.maximumPossibleForce != 0 else { return }
        //print(touch.force / touch.maximumPossibleForce)
        if touch.force / touch.maximumPossibleForce >= threshold {
            state = UIGestureRecognizer.State.began
            //print("State Changed")
        }
        else{
            print("Else Called")
            //state = UIGestureRecognizer.State.changed
        }
        
    }
    
}
