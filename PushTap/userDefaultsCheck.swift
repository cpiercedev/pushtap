//
//  userDefaultsCheck.swift
//  PushTap
//
//  Created by Conor Pierce on 3/24/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import Foundation

func isKeyPresentInUserDefaults(key: String) -> Bool {
    return UserDefaults.standard.object(forKey: key) != nil
}
