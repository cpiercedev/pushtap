//
//  EndGame.swift
//  PushTap
//
//  Created by Conor Pierce on 3/23/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import UIKit
import StoreKit
import LGButton
class EndGame: UIView {
    
    @IBOutlet var LastScore: UILabel!
    @IBOutlet var PlayButton: LGButton!
    @IBOutlet var RemoveAdsButton: LGButton!
    @IBOutlet var MainMenuButton: LGButton!
    @IBOutlet var RateAppButton: LGButton!
    @IBOutlet var firstStar: UIImageView!
    @IBOutlet var secondStar: UIImageView!
    @IBOutlet var thirdStar: UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
    private func setup() {
        fromNib()
        clipsToBounds = true
        layer.cornerRadius = 5
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
