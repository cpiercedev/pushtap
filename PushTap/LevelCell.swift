//
//  LevelCell.swift
//  PushTap
//
//  Created by Conor Pierce on 3/24/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import UIKit

class LevelCell: UICollectionViewCell {
    
    @IBOutlet var LevelNumber: UILabel!
    @IBOutlet var ViewBackground: UIView!
    @IBOutlet var firstStar: UIImageView!
    @IBOutlet var secondStar: UIImageView!
    @IBOutlet var thirdStar: UIImageView!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }
    
    public func configure(with model: LevelModel) {
        //image.image = model.image
        
        let levelDetails = levelChars()
        let userDefaults = UserDefaults.standard
        LevelNumber.text = "\(model.number)"
        var score: Int
        if(isKeyPresentInUserDefaults(key: "scoreLevel\(model.number)")){
            score = userDefaults.integer(forKey: "scoreLevel\(model.number)")
            let starValue = (levelDetails.checkStars(level: model.number, score: score))
            //print("scoreLevel\(model.number)")
            //print(starValue)
            switch starValue {
            case 1:
                firstStar.image = UIImage(named: "StarFill")
                break
            case 2:
                firstStar.image = UIImage(named: "StarFill")
                secondStar.image = UIImage(named: "StarFill")
                break
            case 3:
                firstStar.image = UIImage(named: "StarFill")
                secondStar.image = UIImage(named: "StarFill")
                thirdStar.image = UIImage(named: "StarFill")
                break
            default:
                break
            }
        }
        else{
            
        }
        ViewBackground.backgroundColor = model.viewBackgroundColor
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        //hide or reset anything you want hereafter, for example
        firstStar.image = UIImage(named: "Star")
        secondStar.image = UIImage(named: "Star")
        thirdStar.image = UIImage(named: "Star")
        
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
