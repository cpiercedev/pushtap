//
//  HapticFeedback.swift
//  PushTap
//
//  Created by Conor Pierce on 4/7/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit

public final class HapticFeedback{
    
    
    var mediumImpactFeedbackGenerator: UIImpactFeedbackGenerator?
    var lightImpactFeedbackGenerator: UIImpactFeedbackGenerator?
    var heavyImpactFeedbackGenerator: UIImpactFeedbackGenerator?
    
    init(){
        heavyImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .heavy)
        mediumImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .medium)
        lightImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)
    }
    
    func feedback(style: String){
        switch style {
        case "light":
            lightImpactFeedbackGenerator!.impactOccurred()
            lightImpactFeedbackGenerator?.prepare()
        case "medium":
            mediumImpactFeedbackGenerator!.impactOccurred()
            mediumImpactFeedbackGenerator?.prepare()
        case "heavy":
            heavyImpactFeedbackGenerator!.impactOccurred()
            heavyImpactFeedbackGenerator?.prepare()
        default:
            break
        }
    }
    
    func prepareFeedback(){
        lightImpactFeedbackGenerator?.prepare()
        mediumImpactFeedbackGenerator!.prepare()
        heavyImpactFeedbackGenerator?.prepare()
    }
}
