//
//  CircleProperties.swift
//  PushTap
//
//  Created by Conor Pierce on 4/7/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import Foundation
import SpriteKit

extension SKSpriteNode{
    var CircleProperties: circleProperties? {
        get {
            return self.CircleProperties
        }
        set(newValue) {
            self.CircleProperties = newValue
        }
    }
    convenience init(circleProperties: circleProperties) {
        let CircleProperties = circleProperties
        //print(CircleProperties)
        self.init()
    }
}
struct circleProperties{
    var circleColor: Int?
    var circleColorName: String?
    var circleForce: Int?
    var circleFeedback: String?
}

var CirclePropsList: [circleProperties]{
    var circleProps: [circleProperties] = []
    
    var greenCircle = circleProperties(circleColor: 0, circleColorName: "GreenCircle", circleForce: 10, circleFeedback: "light")
    circleProps.append(greenCircle)
    var yellowCircle = circleProperties(circleColor: 0, circleColorName: "YellowCircle", circleForce: 30, circleFeedback: "light")
    circleProps.append(yellowCircle)
    var orangeCircle = circleProperties(circleColor: 0, circleColorName: "OrangeCircle", circleForce: 50, circleFeedback: "medium")
    circleProps.append(orangeCircle)
    var redCircle = circleProperties(circleColor: 0, circleColorName: "RedCircle", circleForce: 80, circleFeedback: "heavy")
    circleProps.append(redCircle)
    
    return circleProps
}
var TapCirclePropsList: [circleProperties]{
    var circleProps: [circleProperties] = []
    
    var greenCircle = circleProperties(circleColor: 0, circleColorName: "GreenTap", circleForce: 0, circleFeedback: "light")
    circleProps.append(greenCircle)
    var yellowCircle = circleProperties(circleColor: 0, circleColorName: "YellowTap", circleForce: 0, circleFeedback: "light")
    circleProps.append(yellowCircle)
    var orangeCircle = circleProperties(circleColor: 0, circleColorName: "OrangeTap", circleForce: 0, circleFeedback: "light")
    circleProps.append(orangeCircle)
    var redCircle = circleProperties(circleColor: 0, circleColorName: "RedTap", circleForce: 0, circleFeedback: "light")
    circleProps.append(redCircle)
    
    return circleProps
}
var DoubleTapCirclePropsList: [circleProperties]{
    var circleProps: [circleProperties] = []
    
    var greenCircle = circleProperties(circleColor: 0, circleColorName: "GreenDoubleTap", circleForce: 0, circleFeedback: "medium")
    circleProps.append(greenCircle)
    var yellowCircle = circleProperties(circleColor: 0, circleColorName: "YellowDoubleTap", circleForce: 0, circleFeedback: "medium")
    circleProps.append(yellowCircle)
    var orangeCircle = circleProperties(circleColor: 0, circleColorName: "OrangeDoubleTap", circleForce: 0, circleFeedback: "medium")
    circleProps.append(orangeCircle)
    var redCircle = circleProperties(circleColor: 0, circleColorName: "RedDoubleTap", circleForce: 0, circleFeedback: "medium")
    circleProps.append(redCircle)
    
    return circleProps
}
var GestureCirclePropsList: [circleProperties]{
    var circleProps: [circleProperties] = []
    
    var greenCircle = circleProperties(circleColor: 0, circleColorName: "LeftCircle", circleForce: 0, circleFeedback: "light")
    circleProps.append(greenCircle)
    var yellowCircle = circleProperties(circleColor: 0, circleColorName: "RightCircle", circleForce: 0, circleFeedback: "light")
    circleProps.append(yellowCircle)
    var orangeCircle = circleProperties(circleColor: 0, circleColorName: "UpCircle", circleForce: 0, circleFeedback: "medium")
    circleProps.append(orangeCircle)
    var redCircle = circleProperties(circleColor: 0, circleColorName: "DownCircle", circleForce: 0, circleFeedback: "heavy")
    circleProps.append(redCircle)
    
    return circleProps
}

func getCircle(color: String) -> circleProperties{
    for i in 0..<4{
        if(CirclePropsList[i].circleColorName == color){
            return CirclePropsList[i]
        }
        if(TapCirclePropsList[i].circleColorName == color){
            return TapCirclePropsList[i]
        }
        if(DoubleTapCirclePropsList[i].circleColorName == color){
            return DoubleTapCirclePropsList[i]
        }
        if(GestureCirclePropsList[i].circleColorName == color){
            return GestureCirclePropsList[i]
        }
    }
    return circleProperties()
}
