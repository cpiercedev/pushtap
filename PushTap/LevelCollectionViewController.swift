//
//  LevelCollectionViewController.swift
//  PushTap
//
//  Created by Conor Pierce on 3/24/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import UIKit
import BouncyLayout
private let reuseIdentifier = "Cell"

class LevelCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    var cellColor = true
    var colorCount = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        print(getAppearSpeed(levelNumber: 1))
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.collectionView.delegate = self
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        //let layout = BouncyLayout()
        let layout: BouncyLayout = BouncyLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        //layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 20
        
        collectionView!.collectionViewLayout = layout
        
        collectionView.register(UINib.init(nibName: "LevelCell", bundle: nil), forCellWithReuseIdentifier: "LevelCell")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 100.0, height: 100.0)
    }
  
    public override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("\(indexPath.row + 1)", forKey: "PressedLevelNumber")
        
        performSegue(withIdentifier: "SegueToGame", sender: self)
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 100
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LevelCell", for: indexPath) as! LevelCell
    
        // Configure the cell
        
        
        
        let userDefaults = UserDefaults.standard
        
        
        //userDefaults.set((2), forKey: "maxLevelUnlocked")
        let maxLevelUnlocked = userDefaults.integer(forKey: "maxLevelUnlocked")
        if((indexPath.row + 1) > maxLevelUnlocked){
            //print("Above Max Level: \(indexPath.row)")
            
            cell.configure(with: LevelModel(image: #imageLiteral(resourceName: "GreenCircle"), number: (indexPath.row + 1), viewBackgroundColor: UIColor.init(red: 150, green: 165, blue: 165) ))
            cell.isUserInteractionEnabled = false
            cell.alpha = 0.5
            
        }
        else{
           // print("Below Max Level: \(indexPath.row)")
        switch (indexPath.row % 4) {
        case 3:
            cell.configure(with: LevelModel(image: #imageLiteral(resourceName: "GreenCircle"), number: (indexPath.row + 1), viewBackgroundColor: UIColor.init(red: 217, green: 79, blue: 53) ))
            //cell.backgroundColor = UIColor.init(red: 217, green: 79, blue: 53)
            colorCount += 1
            break
        case 2:
            cell.configure(with: LevelModel(image: #imageLiteral(resourceName: "GreenCircle"), number: (indexPath.row + 1), viewBackgroundColor: UIColor.init(red: 234, green: 213, blue: 30) ))
            //cell.backgroundColor = UIColor.init(red: 234, green: 213, blue: 30)
            colorCount += 1
            break
        case 1:
            cell.configure(with: LevelModel(image: #imageLiteral(resourceName: "GreenCircle"), number: (indexPath.row + 1), viewBackgroundColor: UIColor.init(red: 251, green: 182, blue: 34) ))
            //cell.backgroundColor = UIColor.init(red: 251, green: 182, blue: 34)
            colorCount += 1
            break
        case 0:
            cell.configure(with: LevelModel(image: #imageLiteral(resourceName: "GreenCircle"), number: (indexPath.row + 1), viewBackgroundColor: UIColor.init(red: 59, green: 198, blue: 81) ))
            //cell.backgroundColor = UIColor.init(red: 59, green: 198, blue: 81)
            colorCount += 1
            break
        default:
            break
        }
        }
        cell.layer.cornerRadius = 50.0
        cell.ViewBackground.layer.cornerRadius = 50.0
        cell.ViewBackground.clipsToBounds = true
        cell.clipsToBounds = true
        cell.isUserInteractionEnabled = true
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 1, height: 1)
        cell.layer.masksToBounds = false
        if((indexPath.row + 1) > maxLevelUnlocked){
            cell.isUserInteractionEnabled = false
        }
        //cellColor = !cellColor
                return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
