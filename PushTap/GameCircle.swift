//
//  GameCircle.swift
//  PushTap
//
//  Created by Conor Pierce on 3/14/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import UIKit
import SpriteKit
import GameKit

var circle: SKSpriteNode = SKSpriteNode.init(imageNamed: "GreenButton")
private var lastQuadrant: Int?
private var CircleCount: Int?

var circleXMax: CGFloat?
var circleYMax: CGFloat?
var axisBuffer: CGFloat = 30

var forceColors: Dictionary<String, Int>?

var currentCircleProperties: circleProperties?
var touchedCircle: SKNode?

let swipeRightRec = UISwipeGestureRecognizer()
let swipeLeftRec = UISwipeGestureRecognizer()
let swipeUpRec = UISwipeGestureRecognizer()
let swipeDownRec = UISwipeGestureRecognizer()

//taps

let tapRec = UITapGestureRecognizer()
let tapRec2 = UITapGestureRecognizer()


var haptic: HapticFeedback?


var gameView: UIView?

protocol ForceCircleDelegate: class {
    func endGame()
    func updateBorder(Value: CGFloat)
    func incrementScore(amount: Int)
}
class GameCircle: SKSpriteNode {
    
    weak var forceCircleDelegate: ForceCircleDelegate?
    
    convenience init(view: UIView, circleCount: Int) {
        gameView = view
        circleXMax = view.bounds.width * 0.4
        circleYMax = view.bounds.height * 0.4
        CircleCount = circleCount
        haptic = HapticFeedback()
        self.init()
    }
    
    
    override init(texture: SKTexture!, color: UIColor!, size: CGSize) {
        super.init(texture: SKTexture(imageNamed: "GreenButton"), color: color, size: size)
        // Do custom stuff here
    }

    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    
    func createCircle() -> SKSpriteNode{
        
        //Lets get this more automated at a singular point.
        var circleSize = CGFloat.random(in: 150.0..<230.0)
        
        //selecting one of the four options for circle creation
        let random = Int.random(in: 0..<4)
        
        currentCircleProperties = CirclePropsList[random]
        
        circle = SKSpriteNode.init(circleProperties: currentCircleProperties!)
        
        circle.size = CGSize(width: circleSize, height: circleSize)
        
        circle.texture = SKTexture(imageNamed: "\((currentCircleProperties?.circleColorName)!)")
        
        circle.name = currentCircleProperties!.circleColorName
        
        circle.position = getPosition()
        
        return circle
        
    }
    
    func createTapCircle() -> SKSpriteNode{
        var circleSize = CGFloat.random(in: 150.0..<230.0)
        let random = Int.random(in: 0..<4)
         currentCircleProperties = TapCirclePropsList[random]
        circle = SKSpriteNode.init(circleProperties: currentCircleProperties!)
        
        circle.size = CGSize(width: circleSize, height: circleSize)
        
        circle.texture = SKTexture(imageNamed: "\((currentCircleProperties?.circleColorName)!)")
        
        circle.name = "SingleTap"
        circle.position = getPosition()
        return circle
    }
    func createDoubleTapCircle() -> SKSpriteNode{
        
        var circleSize = CGFloat.random(in: 150.0..<230.0)
        let random = Int.random(in: 0..<4)
         currentCircleProperties = DoubleTapCirclePropsList[random]
        circle = SKSpriteNode.init(circleProperties: currentCircleProperties!)
        
        circle.size = CGSize(width: circleSize, height: circleSize)
        
        circle.texture = SKTexture(imageNamed: "\((currentCircleProperties?.circleColorName)!)")
        
        circle.name = "DoubleTap"
        circle.position = getPosition()
        return circle
    }
    func createGestureCircle() -> SKSpriteNode{
        var circleSize = CGFloat.random(in: 150.0..<230.0)
        let random = Int.random(in: 0..<4)
        currentCircleProperties = GestureCirclePropsList[random]
        circle = SKSpriteNode.init(circleProperties: currentCircleProperties!)
        
        circle.size = CGSize(width: circleSize, height: circleSize)
        
        circle.texture = SKTexture(imageNamed: "\((currentCircleProperties?.circleColorName)!)")
        
        circle.name = currentCircleProperties!.circleColorName
        
        circle.position = getPosition()
        
        swipeRightRec.addTarget(self, action: #selector(GameCircle.swipedRight) )
        swipeRightRec.direction = .right
        gameView!.addGestureRecognizer(swipeRightRec)
        
        swipeLeftRec.addTarget(self, action: #selector(GameCircle.swipedLeft) )
        swipeLeftRec.direction = .left
        gameView!.addGestureRecognizer(swipeLeftRec)
        
        
        swipeUpRec.addTarget(self, action: #selector(GameCircle.swipedUp) )
        swipeUpRec.direction = .up
        gameView!.addGestureRecognizer(swipeUpRec)
        
        swipeDownRec.addTarget(self, action: #selector(GameCircle.swipedDown) )
        swipeDownRec.direction = .down
        gameView!.addGestureRecognizer(swipeDownRec)
        
        
        return circle
    }
    func startShrink(){
        let userDefaults = UserDefaults.standard
        let currentLevel = userDefaults.integer(forKey: "PressedLevelNumber")
        let popSpeed = getPopSpeed(levelNumber: 1)
        
        let callEnd = SKAction.run {
            self.forceCircleDelegate?.endGame()
        }
        let shrink = SKAction.scale(to: 0, duration: popSpeed)
        let removeNode = SKAction.removeFromParent()
        let fadeout = SKAction.fadeAlpha(to: 0, duration: 2)
        
        let sequence = SKAction.sequence([shrink, callEnd, fadeout, removeNode])
        
        circle.run(sequence)
    }
    
    func setTouchedCircle(TouchedCircle: SKNode){
        touchedCircle = TouchedCircle
    }
    
    @objc func getForceValue() -> Int{
        let color = circle.name
        return forceColors?.index(forKey: "\(color)") as! Int
    }
    @objc func swipedRight() {
        
        print("Right")
        print(touchedCircle?.name)
        if(touchedCircle!.name == nil){
            return
        }
        if(touchedCircle!.name == "RightCircle"){
            touchedCircle!.removeFromParent()
            haptic?.feedback(style: "medium")
            self.forceCircleDelegate?.updateBorder(Value: 0.0)
            self.forceCircleDelegate?.incrementScore(amount: 1)
        }
    }
    
    @objc func swipedLeft() {
        
        print("Left")
        print(touchedCircle?.name)
        if(touchedCircle!.name == nil){
            return
        }
        if(touchedCircle!.name == "LeftCircle"){
            touchedCircle!.removeFromParent()
            haptic?.feedback(style: "medium")
            self.forceCircleDelegate?.updateBorder(Value: 0.0)
            self.forceCircleDelegate?.incrementScore(amount: 1)
        }
    }
    
    @objc func swipedUp() {
        
        print("Up")
        print(touchedCircle?.name)
        if(touchedCircle!.name == nil){
            return
        }
        if(touchedCircle!.name == "UpCircle"){
            touchedCircle!.removeFromParent()
            haptic?.feedback(style: "medium")
            self.forceCircleDelegate?.updateBorder(Value: 0.0)
            self.forceCircleDelegate?.incrementScore(amount: 1)
        }
    }
    
    @objc  func swipedDown() {
        
        print("Down")
        print(touchedCircle?.name)
        if(touchedCircle!.name == nil){
            return
        }
        if(touchedCircle!.name == "DownCircle"){
            touchedCircle!.removeFromParent()
            haptic?.feedback(style: "medium")
            self.forceCircleDelegate?.updateBorder(Value: 0.0)
            self.forceCircleDelegate?.incrementScore(amount: 1)
        }
    }
}
