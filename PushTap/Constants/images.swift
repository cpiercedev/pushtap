//
//  images.swift
//  PushTap
//
//  Created by Conor Pierce on 4/9/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit.UIImage

internal typealias AssetColorTypeAlias = UIColor
internal typealias AssetImageTypeAlias = UIImage

internal enum Asset {
    internal static let banks = ImageAsset(name: "YellowCircle")
    internal static let hotels = ImageAsset(name: "GreenCircle")
    internal static let key = ImageAsset(name: "OrangeCircle")
    internal static let shoppingCart = ImageAsset(name: "RedCircle")
    internal static let stores = ImageAsset(name: "OrangeCircle")
    internal static let wallet = ImageAsset(name: "GreenCircle")
    
    
    internal static let greenCircle = ImageAsset(name: "GreenCircle")
    internal static let yellowCircle = ImageAsset(name: "YellowCircle")
    internal static let orangeCircle = ImageAsset(name: "OrangeCircle")
    internal static let redCircle = ImageAsset(name: "RedCircle")
    
}

internal struct ImageAsset {
    internal fileprivate(set) var name: String
    
    internal var image: AssetImageTypeAlias {
        let bundle = Bundle(for: BundleToken.self)
        #if os(iOS) || os(tvOS)
        let image = AssetImageTypeAlias(named: name, in: bundle, compatibleWith: nil)
        #elseif os(OSX)
        let image = bundle.image(forResource: name)
        #elseif os(watchOS)
        let image = AssetImageTypeAlias(named: name)
        #endif
        guard let result = image else { fatalError("Unable to load image named \(name).") }
        return result
    }
}

private final class BundleToken {}
