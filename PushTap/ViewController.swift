//
//  ViewController.swift
//  PushTap
//
//  Created by Conor Pierce on 3/14/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import UIKit
import SpriteKit
import InteractiveSideMenu
import SwiftEntryKit
import Foundation
import StoreKit

import QuickLayout


protocol StartGameDelegate: class{
    func startGame()
}

class ViewController: UIViewController, GameSceneDelegate {
    
    weak var startGameDelegate: StartGameDelegate?
    
    func segue() {
        scene?.removeFromParent()
        self.dismiss(animated: true, completion: nil)
        
    }
    func showPopOver(score: Int){
    
        let levelDetails = levelChars()
        var attributes: EKAttributes
        var descriptionString: String
        let userDefaults = UserDefaults.standard
        let currentLevel = userDefaults.integer(forKey: "PressedLevelNumber")
        
        
        
        if(isKeyPresentInUserDefaults(key: "scoreLevel\(currentLevel)")){
            let currentScore = userDefaults.integer(forKey: "scoreLevel\(currentLevel)")
            if(currentScore < score){
                userDefaults.set(score, forKey: "scoreLevel\(currentLevel)")
            }
        }
        else{
            userDefaults.set(score, forKey: "scoreLevel\(currentLevel)")
        }
        
        
        
        attributes = .centerFloat
        attributes.windowLevel = .normal
        attributes.hapticFeedbackType = .success
        attributes.displayDuration = .infinity
        attributes.screenInteraction = .absorbTouches
        
        attributes.entryInteraction = .absorbTouches
        attributes.roundCorners = .all(radius: 20)
        
        attributes.entryBackground = .color(color: UIColor.white.withAlphaComponent(0.98))
        attributes.screenBackground = .visualEffect(style: .light)
        
        attributes.entranceAnimation = .init(translate: .init(duration: 0.5, spring: .init(damping: 0.9, initialVelocity: 0)),
                                             scale: .init(from: 0.8, to: 1, duration: 0.5, spring: .init(damping: 0.8, initialVelocity: 0)),
                                             fade: .init(from: 0.7, to: 1, duration: 0.3))
        attributes.exitAnimation = .init(translate: .init(duration: 0.5),
                                         scale: .init(from: 1, to: 0.8, duration: 0.5),
                                         fade: .init(from: 1, to: 0, duration: 0.5))
        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3),
                                                            scale: .init(from: 1, to: 0.8, duration: 0.3)))
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.3, radius: 6))
        
        attributes.positionConstraints.verticalOffset = 10
        attributes.positionConstraints.size = .init(width: .offset(value: 20), height: .intrinsic)
        attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
        
        attributes.statusBar = .ignored
        descriptionString = "Customized view that is initialized by a nib file, it is additionally added various attributes such as round corners and a mild shadow"
        let EndGameView = EndGame()
        EndGameView.LastScore.text = "\(score)"
        let starValue = (levelDetails.checkStars(level: currentLevel, score: score))
        if(starValue > 0){
            let unlockedLevel = userDefaults.integer(forKey: "maxLevelUnlocked")
            print("Current Level: \(currentLevel)")
            print("unlocked Level: \(unlockedLevel)")
            if(currentLevel == unlockedLevel){
                userDefaults.set((unlockedLevel + 1), forKey: "maxLevelUnlocked")
            }
        }
        switch starValue {
        case 1:
            EndGameView.firstStar.image = UIImage(named: "StarBlackFill")
            break
        case 2:
            EndGameView.firstStar.image = UIImage(named: "StarBlackFill")
            EndGameView.secondStar.image = UIImage(named: "StarBlackFill")
            break
        case 3:
            EndGameView.firstStar.image = UIImage(named: "StarBlackFill")
            EndGameView.secondStar.image = UIImage(named: "StarBlackFill")
            EndGameView.thirdStar.image = UIImage(named: "StarBlackFill")
            break
        default:
            EndGameView.RemoveAdsButton.isEnabled = false
            break
            
        }
       
    

            
        
        
        EndGameView.PlayButton.addTarget(self,
                                     action: #selector(playButtonPressed),
                                     for: .touchUpInside)
        EndGameView.RemoveAdsButton.addTarget(self,
                                          action: #selector(removeAdsButtonPressed),
                                          for: .touchUpInside)
        EndGameView.MainMenuButton.addTarget(self,
                                         action: #selector(mainMenuButtonPressed),
                                         for: .touchUpInside)
        EndGameView.RateAppButton.addTarget(self,
                                        action: #selector(rateAppButtonPressed),
                                        for: .touchUpInside)
        
        
        SwiftEntryKit.display(entry: EndGameView, using: attributes)
    }
    
    
    var scene: GameScene?
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            
            if (self.view as! SKView?) != nil {
                self.scene = GameScene(fileNamed: "GameScene")
            }
            
            self.scene?.gameDelegate = self
            scene?.scaleMode = .resizeFill
            scene?.backgroundColor = .white
            view.presentScene(scene)
            
            /*
             if scene = SKScene(fileNamed: "GameScene"){
             
             // Set the scale mode to scale to fit the window
             scene.scaleMode = .aspectFill
             // Present the scene
             self.scene?.gameDelegate = self
             view.presentScene(scene)
             }
             */
            view.ignoresSiblingOrder = true
            
            //view.showsFPS = true
            //view.showsNodeCount = true
        }
    }
    @objc func playButtonPressed(){
        SwiftEntryKit.dismiss(.all)
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            
            if (self.view as! SKView?) != nil {
                self.scene = GameScene(fileNamed: "GameScene")
            }
            
            self.scene?.gameDelegate = self
            scene?.scaleMode = .resizeFill
            scene?.backgroundColor = .white
            view.presentScene(scene)
            
            /*
             if scene = SKScene(fileNamed: "GameScene"){
             
             // Set the scale mode to scale to fit the window
             scene.scaleMode = .aspectFill
             // Present the scene
             self.scene?.gameDelegate = self
             view.presentScene(scene)
             }
             */
            view.ignoresSiblingOrder = true
            
            //view.showsFPS = true
            //view.showsNodeCount = true
        }
        
    }
    @objc func removeAdsButtonPressed(){
        //Need to add the code for an in app purchase
        let userDefaults = UserDefaults.standard
        let currentLevel = userDefaults.integer(forKey: "PressedLevelNumber") + 1
        userDefaults.set(currentLevel, forKey: "PressedLevelNumber")
        SwiftEntryKit.dismiss(.all)
        
        self.startGameDelegate?.startGame()
        
        
    }
    @objc func mainMenuButtonPressed(){
        
        self.performSegue(withIdentifier: "SegueToMainMenu", sender: self)
        SwiftEntryKit.dismiss(.all)
    }
    @objc func rateAppButtonPressed(){
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else {
            //Use your custom review view.
        }
    }


}

