//
//  LoadGameData.swift
//  PushTap
//
//  Created by Conor Pierce on 4/8/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import Foundation


func loadGameData(){
    
}
func getLevelData(levelNumber: Int){
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory = paths.object(at: 0) as! NSString
    let path = documentsDirectory.appendingPathComponent("GameData.plist")
    
    let fileManager = FileManager.default
    
    //check if file exists
    if !fileManager.fileExists(atPath: path) {
        
        guard let bundlePath = Bundle.main.path(forResource: "GameData", ofType: "plist") else { return }
        
        do {
            try fileManager.copyItem(atPath: bundlePath, toPath: path)
        } catch let error as NSError {
            print("Unable to copy file. ERROR: \(error.localizedDescription)")
        }
    }
    
    let resultDictionary = NSMutableDictionary(contentsOfFile: path)
    print("Loaded GameData.plist file is --> \(resultDictionary?.description ?? "")")
    
    let myDict = NSDictionary(contentsOfFile: path)
    //print(myDict)
    var levelData = myDict?.value(forKey: ("\(levelNumber)"))
//    print(levelData)
    let dog: [String] = []
    return
//    if let dict = myDict {
//        //loading values
//        bedroomFloorID = dict.object(forKey: BedroomFloorKey)!
//        bedroomWallID = dict.object(forKey: BedroomWallKey)!
//        //...
//    } else {
//        print("WARNING: Couldn't create dictionary from GameData.plist! Default values will be used!")
//    }
}

func getAppearSpeed(levelNumber: Int) -> Double{
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory = paths.object(at: 0) as! NSString
    let path = documentsDirectory.appendingPathComponent("GameData.plist")
    
    let fileManager = FileManager.default
    
    //check if file exists
    if !fileManager.fileExists(atPath: path) {
        
        guard let bundlePath = Bundle.main.path(forResource: "GameData", ofType: "plist") else { return 0 }
        
        do {
            try fileManager.copyItem(atPath: bundlePath, toPath: path)
        } catch let error as NSError {
            print("Unable to copy file. ERROR: \(error.localizedDescription)")
        }
    }
    
    let resultDictionary = NSMutableDictionary(contentsOfFile: path)
    print("Loaded GameData.plist file is --> \(resultDictionary?.description ?? "")")
    
    let myDict = NSDictionary(contentsOfFile: path)
    let levelattributes: NSDictionary = myDict?.value(forKey: "\(levelNumber)") as! NSDictionary
    let appearSpeed = levelattributes.object(forKey: "AppearSpeed") as! Double
    
    return appearSpeed
}


func getPopSpeed(levelNumber: Int) -> Double{
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory = paths.object(at: 0) as! NSString
    let path = documentsDirectory.appendingPathComponent("GameData.plist")
    
    let fileManager = FileManager.default
    
    //check if file exists
    if !fileManager.fileExists(atPath: path) {
        
        guard let bundlePath = Bundle.main.path(forResource: "GameData", ofType: "plist") else { return 0 }
        
        do {
            try fileManager.copyItem(atPath: bundlePath, toPath: path)
        } catch let error as NSError {
            print("Unable to copy file. ERROR: \(error.localizedDescription)")
        }
    }
    
    let resultDictionary = NSMutableDictionary(contentsOfFile: path)
    print("Loaded GameData.plist file is --> \(resultDictionary?.description ?? "")")
    
    let myDict = NSDictionary(contentsOfFile: path)
    let levelattributes: NSDictionary = myDict?.value(forKey: "\(levelNumber)") as! NSDictionary
    let data = levelattributes.object(forKey: "PopSpeed") as! Double
    
    return data
}


func getDifficultyIncrement(levelNumber: Int) -> Double{
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory = paths.object(at: 0) as! NSString
    let path = documentsDirectory.appendingPathComponent("GameData.plist")
    
    let fileManager = FileManager.default
    
    //check if file exists
    if !fileManager.fileExists(atPath: path) {
        
        guard let bundlePath = Bundle.main.path(forResource: "GameData", ofType: "plist") else { return 0 }
        
        do {
            try fileManager.copyItem(atPath: bundlePath, toPath: path)
        } catch let error as NSError {
            print("Unable to copy file. ERROR: \(error.localizedDescription)")
        }
    }
    
    let resultDictionary = NSMutableDictionary(contentsOfFile: path)
    print("Loaded GameData.plist file is --> \(resultDictionary?.description ?? "")")
    
    let myDict = NSDictionary(contentsOfFile: path)
    let levelattributes: NSDictionary = myDict?.value(forKey: "\(levelNumber)") as! NSDictionary
    let data = levelattributes.object(forKey: "DifficultyIncrement") as! Double
    
    return data
}

func getHighScore(levelNumber: Int) -> Int{
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory = paths.object(at: 0) as! NSString
    let path = documentsDirectory.appendingPathComponent("GameData.plist")
    
    let fileManager = FileManager.default
    
    //check if file exists
    if !fileManager.fileExists(atPath: path) {
        
        guard let bundlePath = Bundle.main.path(forResource: "GameData", ofType: "plist") else { return 0 }
        
        do {
            try fileManager.copyItem(atPath: bundlePath, toPath: path)
        } catch let error as NSError {
            print("Unable to copy file. ERROR: \(error.localizedDescription)")
        }
    }
    
    let resultDictionary = NSMutableDictionary(contentsOfFile: path)
    print("Loaded GameData.plist file is --> \(resultDictionary?.description ?? "")")
    
    let myDict = NSDictionary(contentsOfFile: path)
    let levelattributes: NSDictionary = myDict?.value(forKey: "\(levelNumber)") as! NSDictionary
    let data = levelattributes.object(forKey: "HighScore") as! Int
    
    return data
}

func hasForce(levelNumber: Int) -> Bool{
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory = paths.object(at: 0) as! NSString
    let path = documentsDirectory.appendingPathComponent("GameData.plist")
    
    let fileManager = FileManager.default
    
    //check if file exists
    if !fileManager.fileExists(atPath: path) {
        
        guard let bundlePath = Bundle.main.path(forResource: "GameData", ofType: "plist") else { return false }
        
        do {
            try fileManager.copyItem(atPath: bundlePath, toPath: path)
        } catch let error as NSError {
            print("Unable to copy file. ERROR: \(error.localizedDescription)")
        }
    }
    
    let resultDictionary = NSMutableDictionary(contentsOfFile: path)
    print("Loaded GameData.plist file is --> \(resultDictionary?.description ?? "")")
    
    let myDict = NSDictionary(contentsOfFile: path)
    let levelattributes: NSDictionary = myDict?.value(forKey: "\(levelNumber)") as! NSDictionary
    let data = levelattributes.object(forKey: "ForceCircle") as! Bool
    
    return data
}


func hasDoubleTap(levelNumber: Int) -> Bool{
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory = paths.object(at: 0) as! NSString
    let path = documentsDirectory.appendingPathComponent("GameData.plist")
    
    let fileManager = FileManager.default
    
    //check if file exists
    if !fileManager.fileExists(atPath: path) {
        
        guard let bundlePath = Bundle.main.path(forResource: "GameData", ofType: "plist") else { return false }
        
        do {
            try fileManager.copyItem(atPath: bundlePath, toPath: path)
        } catch let error as NSError {
            print("Unable to copy file. ERROR: \(error.localizedDescription)")
        }
    }
    
    let resultDictionary = NSMutableDictionary(contentsOfFile: path)
    print("Loaded GameData.plist file is --> \(resultDictionary?.description ?? "")")
    
    let myDict = NSDictionary(contentsOfFile: path)
    let levelattributes: NSDictionary = myDict?.value(forKey: "\(levelNumber)") as! NSDictionary
    let DoubleTapCircle = levelattributes.object(forKey: "AppearSpeed") as! Bool
    
    return DoubleTapCircle
}


func hasSingleTap(levelNumber: Int) -> Bool{
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory = paths.object(at: 0) as! NSString
    let path = documentsDirectory.appendingPathComponent("GameData.plist")
    
    let fileManager = FileManager.default
    
    //check if file exists
    if !fileManager.fileExists(atPath: path) {
        
        guard let bundlePath = Bundle.main.path(forResource: "GameData", ofType: "plist") else { return false }
        
        do {
            try fileManager.copyItem(atPath: bundlePath, toPath: path)
        } catch let error as NSError {
            print("Unable to copy file. ERROR: \(error.localizedDescription)")
        }
    }
    
    let resultDictionary = NSMutableDictionary(contentsOfFile: path)
    print("Loaded GameData.plist file is --> \(resultDictionary?.description ?? "")")
    
    let myDict = NSDictionary(contentsOfFile: path)
    let levelattributes: NSDictionary = myDict?.value(forKey: "\(levelNumber)") as! NSDictionary
    let SingleTapCircle = levelattributes.object(forKey: "TapCircle") as! Bool
    
    return SingleTapCircle
}


func hasGesture(levelNumber: Int) -> Bool{
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory = paths.object(at: 0) as! NSString
    let path = documentsDirectory.appendingPathComponent("GameData.plist")
    
    let fileManager = FileManager.default
    
    //check if file exists
    if !fileManager.fileExists(atPath: path) {
        
        guard let bundlePath = Bundle.main.path(forResource: "GameData", ofType: "plist") else { return false }
        
        do {
            try fileManager.copyItem(atPath: bundlePath, toPath: path)
        } catch let error as NSError {
            print("Unable to copy file. ERROR: \(error.localizedDescription)")
        }
    }
    
    let resultDictionary = NSMutableDictionary(contentsOfFile: path)
    print("Loaded GameData.plist file is --> \(resultDictionary?.description ?? "")")
    
    let myDict = NSDictionary(contentsOfFile: path)
    let levelattributes: NSDictionary = myDict?.value(forKey: "\(levelNumber)") as! NSDictionary
    let data = levelattributes.object(forKey: "GestureCircle") as! Bool
    
    return data
}

func getCircleWeights(levelNumber: Int) -> [Double]{
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory = paths.object(at: 0) as! NSString
    let path = documentsDirectory.appendingPathComponent("GameData.plist")
    
    let fileManager = FileManager.default
    
    //check if file exists
    if !fileManager.fileExists(atPath: path) {
        
        guard let bundlePath = Bundle.main.path(forResource: "GameData", ofType: "plist") else { return [0,0,0] }
        
        do {
            try fileManager.copyItem(atPath: bundlePath, toPath: path)
        } catch let error as NSError {
            print("Unable to copy file. ERROR: \(error.localizedDescription)")
        }
    }
    
    let resultDictionary = NSMutableDictionary(contentsOfFile: path)
    print("Loaded GameData.plist file is --> \(resultDictionary?.description ?? "")")
    
    let myDict = NSDictionary(contentsOfFile: path)
    let levelattributes: NSDictionary = myDict?.value(forKey: "\(levelNumber)") as! NSDictionary
    print(levelattributes)
    let data = levelattributes.object(forKey: "Weights") as! [Double]
    
    
    return data
}
func getStarScores(levelNumber: Int) -> [Int]{
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory = paths.object(at: 0) as! NSString
    let path = documentsDirectory.appendingPathComponent("GameData.plist")
    
    let fileManager = FileManager.default
    
    //check if file exists
    if !fileManager.fileExists(atPath: path) {
        
        guard let bundlePath = Bundle.main.path(forResource: "GameData", ofType: "plist") else { return [0,0,0] }
        
        do {
            try fileManager.copyItem(atPath: bundlePath, toPath: path)
        } catch let error as NSError {
            print("Unable to copy file. ERROR: \(error.localizedDescription)")
        }
    }
    
    let resultDictionary = NSMutableDictionary(contentsOfFile: path)
    print("Loaded GameData.plist file is --> \(resultDictionary?.description ?? "")")
    
    let myDict = NSDictionary(contentsOfFile: path)
    let levelattributes: NSDictionary = myDict?.value(forKey: "\(levelNumber)") as! NSDictionary
    let data1 = levelattributes.object(forKey: "OneStar") as! Int
    let data2 = levelattributes.object(forKey: "TwoStar") as! Int
    let data3 = levelattributes.object(forKey: "ThreeStar") as! Int
    
    return [data1, data2, data3]
}

func getStars(levelNumber: Int) -> Int{
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
    let documentsDirectory = paths.object(at: 0) as! NSString
    let path = documentsDirectory.appendingPathComponent("GameData.plist")
    
    let fileManager = FileManager.default
    
    //check if file exists
    if !fileManager.fileExists(atPath: path) {
        
        guard let bundlePath = Bundle.main.path(forResource: "GameData", ofType: "plist") else { return 0 }
        
        do {
            try fileManager.copyItem(atPath: bundlePath, toPath: path)
        } catch let error as NSError {
            print("Unable to copy file. ERROR: \(error.localizedDescription)")
        }
    }
    
    let resultDictionary = NSMutableDictionary(contentsOfFile: path)
    print("Loaded GameData.plist file is --> \(resultDictionary?.description ?? "")")
    
    let myDict = NSDictionary(contentsOfFile: path)
    let levelattributes: NSDictionary = myDict?.value(forKey: "\(levelNumber)") as! NSDictionary
    let data = levelattributes.object(forKey: "Stars") as! Int
    return data
}
