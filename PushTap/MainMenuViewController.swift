//
//  MenuViewController.swift
//  PushTap
//
//  Created by Conor Pierce on 3/19/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import UIKit
import Lottie
import SwiftEntryKit
import QuickLayout
import Foundation


class MainMenuViewController: UIViewController {
    @IBOutlet var StartButton: AnimatedButton!
    @IBOutlet var AnimationView: UIView!
    
    @IBOutlet var AnimationLogo: AnimationView!
    
    // @IBOutlet var StartButton: LOTAnimationView!
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    
        
       
    }
    override func viewDidAppear(_ animated: Bool) {
        let userDefaults = UserDefaults.standard
        if(!isKeyPresentInUserDefaults(key: "maxLevelUnlocked")){
            userDefaults.set(3, forKey: "maxLevelUnlocked")
        }
         AnimationLogo.play()
}
    
}
