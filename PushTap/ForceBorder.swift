//
//  ForceBorder.swift
//  PushTap
//
//  Created by Conor Pierce on 3/14/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import UIKit
import SpriteKit

class ForceBorder: SKSpriteNode {
    private var xMax: CGFloat?
    private var yMax: CGFloat?
    private var forceBorder1: SKSpriteNode?
    private var forceBorder2: SKSpriteNode?
    private var forceBorder3: SKSpriteNode?
    private var forceBorder4: SKSpriteNode?
    private var borderImages: [String]?
    
    convenience init(view: UIView) {
        self.init()
        borderImages = ["GreenBorder","YellowBorder","OrangeBorder","RedBorder"]
        self.xMax = view.bounds.width
        self.yMax = view.bounds.height
        forceBorder1 = SKSpriteNode.init(imageNamed: borderImages![0])
        forceBorder2 = SKSpriteNode.init(imageNamed: borderImages![1])
        forceBorder3 = SKSpriteNode.init(imageNamed: borderImages![2])
        forceBorder4 = SKSpriteNode.init(imageNamed: borderImages![3])
        
        forceBorder1?.alpha = 0
        forceBorder2?.alpha = 0
        forceBorder3?.alpha = 0
        forceBorder4?.alpha = 0
        
        var borderSize = CGSize(width: xMax! * 1.2, height: yMax! * 1.1)
        forceBorder1?.size = borderSize
        forceBorder2?.size = borderSize
        forceBorder3?.size = borderSize
        forceBorder4?.size = borderSize
        
        
    }
    override init(texture: SKTexture!, color: UIColor!, size: CGSize) {
        super.init(texture: SKTexture(imageNamed: "GreenButton"), color: color, size: size)
        // Do custom stuff here
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func updateBorder(ForceValue: CGFloat){
        if(ForceValue == 0){
            forceBorder1?.alpha = 0
            forceBorder2?.alpha = 0
            forceBorder3?.alpha = 0
            forceBorder4?.alpha = 0
        }
        if( ForceValue > 10 && ForceValue <= 20){
            var greenAlpha = ForceValue * 0.03;
            forceBorder1!.alpha = greenAlpha;
            forceBorder2!.alpha = 0;
            forceBorder3!.alpha = 0;
            forceBorder4!.alpha = 0;
        }
            //fade from green to yellow
        else if(ForceValue >= 20 && ForceValue < 30){
            forceBorder1!.alpha = (30-ForceValue) * 0.05;
            forceBorder2!.alpha = (ForceValue - 20) * 0.04;
            forceBorder3!.alpha = 0;
            forceBorder4!.alpha = 0;
        }
            //yellow to orange
        else if(ForceValue >= 40 && ForceValue < 60){
            forceBorder1!.alpha = 0;
            forceBorder2!.alpha = (50-ForceValue) * 0.05;
            forceBorder3!.alpha = (ForceValue - 40) * 0.04;
            forceBorder4!.alpha = 0;
        }
            //fade from orange to red
        else if(ForceValue >= 60 && ForceValue < 80){
            forceBorder1!.alpha = 0;
            forceBorder2!.alpha = 0;
            forceBorder3!.alpha = (70-ForceValue) * 0.05;
            forceBorder4!.alpha = (ForceValue - 60) * 0.03;
        }
            //red
        else if(ForceValue > 80){
            forceBorder1!.alpha = 0;
            forceBorder2!.alpha = 0;
            forceBorder3!.alpha = 0;
            forceBorder4!.alpha = (ForceValue - 70) * 0.03;
        }
    }
    
    func getBorder1() -> SKSpriteNode{
        return forceBorder1!
    }
    func getBorder2() -> SKSpriteNode{
        return forceBorder2!
    }
    func getBorder3() -> SKSpriteNode{
        return forceBorder3!
    }
    func getBorder4() -> SKSpriteNode{
        return forceBorder4!
    }
}
